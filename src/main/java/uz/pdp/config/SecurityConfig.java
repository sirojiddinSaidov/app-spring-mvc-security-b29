package uz.pdp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity.csrf().disable();
//        httpSecurity.authorizeHttpRequests()
//                .requestMatchers(HttpMethod.GET, "/api/payment/*", "/api/user/**", "/api/student")
//                .permitAll()
//                .requestMatchers(HttpMethod.POST, "/api/user").permitAll()
//                .requestMatchers("/api/**").authenticated();


        httpSecurity.authorizeHttpRequests(matcherRegistry -> matcherRegistry
                        .requestMatchers("/test", "/auth/sign-in").permitAll()
                        .anyRequest().authenticated())
                .formLogin()
//                .loginPage("/auth/sign-in")
//                .usernameParameter("phone")
//                .passwordParameter("parol")
//                .defaultSuccessUrl("/cabinet", true)
//                .and()
//                .logout()
//                .logoutUrl("/user/logout")
//                .logoutRequestMatcher(new AntPathRequestMatcher("/user/logout", "POST"))
//                .logoutSuccessUrl("/auth/sign-in")
//                .deleteCookies("JSESSIONID");
        ;

        return httpSecurity.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails user1 = User
                .withDefaultPasswordEncoder()
                .password("123")
                .username("user")
                .authorities("ROLE_USER")
                .build();

        UserDetails user2 = User
                .withDefaultPasswordEncoder()
                .password("123")
                .username("admin")
                .authorities("ROLE_ADMIN")
                .build();
        return new InMemoryUserDetailsManager(user1, user2);
    }
}
