package uz.pdp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestController {

    @GetMapping
    public String first() {
        return "first";
    }

    @GetMapping("/second")
    public String second() {
        return "second";
    }
}
