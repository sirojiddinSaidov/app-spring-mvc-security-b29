package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class HomeController {

    @GetMapping("/home")
    public String getHomePage() {
        return "home";
    }

    @GetMapping("/test")
    @ResponseBody
    public String getTest() {
        return "test";
    }

    @GetMapping("/cabinet")
    @ResponseBody
    public String defaultPage() {
        return "Cabinet";
    }


    @GetMapping("/user/logout")
    public String loginPage() {
        return "logout";
    }

    @GetMapping("/payment")
    @ResponseBody
    public String payment() {
        return "/payment";
    }

    @GetMapping("/payment/")
    @ResponseBody
    public String paymentSlash() {
        return "/payment/";
    }

    @GetMapping("/payment/bla")
    @ResponseBody
    public String paymentBla() {
        return "/payment/bla";
    }

    @GetMapping("/user")
    @ResponseBody
    public String user() {
        return "/user";
    }

    @GetMapping("/user/bla")
    @ResponseBody
    public String userBla() {
        return "/user/bla";
    }

    @GetMapping("/user/bla/battar")
    @ResponseBody
    public String userBlaBattar() {
        return "/user/bla/battar";
    }

    @GetMapping("/user/bla/battar/balo")
    @ResponseBody
    public String userBlaBattarBalo() {
        return "/user/bla/battar/balo";
    }

    @GetMapping("/student")
    @ResponseBody
    public String student() {
        return "/student";
    }

    @GetMapping("/student/bla")
    @ResponseBody
    public String studentBla() {
        return "/student/bla";
    }

    @PostMapping("/user")
    @ResponseBody
    public String postUser() {
        return "postUser";
    }

    @PostMapping("/student")
    @ResponseBody
    public String postStudent() {
        return "postStudent";
    }
}
