package uz.pdp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.model.Ketmon;

@Controller
@RequestMapping("/auth")
public class AuthController {

    @GetMapping("/sign-in")
    public String signInPage() {
        return "signIn";
    }

    @RequestMapping("/reg")
    @ResponseBody
    public String reg(@RequestBody Ketmon ketmon) {
        System.out.println(ketmon);
        return "qalay";
    }
}
